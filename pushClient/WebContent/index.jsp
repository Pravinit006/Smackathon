<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Push Client</title>
<%@ page import=" java.lang.String ,java.util.ArrayList , java.util.List" %>
<%@ page import="org.codehaus.jackson.JsonFactory , org.codehaus.jettison.json.JSONObject , com.sun.jersey.api.client.Client, com.sun.jersey.api.client.WebResource" %>
<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Roboto+Condensed:300); 
body {
    background-color: #F0B445;
}

h2, h4 {
    color: #fff;
    font-family: 'Roboto Condensed', sans-serif;
    text-align: center;
    font-weight: 300;
    text-transform: uppercase;
}

button {
    border: 0px;
    padding: 10px;
    background-color: #fff;
    box-shadow: none;
    margin-left: auto;
    margin-right: auto;
    display: block;
    transition: all 0.3s ease;
}

button:hover {
    background-color: #2980b9;
    cursor: pointer;
    color: #fff;
}

.container{
  margin-top: 20px;
}

</style>
<script type="text/javascript">
<%
String url = "http://internet.localhost:9080/rpsWSS/pushService";
org.codehaus.jettison.json.JSONObject pushRequest = new org.codehaus.jettison.json.JSONObject();
org.codehaus.jettison.json.JSONObject pushResponse = new org.codehaus.jettison.json.JSONObject();
com.sun.jersey.api.client.Client client = com.sun.jersey.api.client.Client.create();
com.sun.jersey.api.client.WebResource webResource = client.resource(url); 
%>
<%  
try {
	pushRequest.put("appId", "6943RPS_04");
	pushRequest.put("userId" , "U0001");
	pushResponse =webResource.accept("application/json").type("application/json").post(org.codehaus.jettison.json.JSONObject.class, pushRequest);
	System.out.println("Request JSON : \n "+pushRequest.toString());
	System.out.println("Response JSON : \n "+pushResponse.toString());
}
catch(Exception ex)
{
	ex.printStackTrace();
}

%> 

function shunno_push_api() {
  if (!Notification) {
    alert('Oops something went wrong!'); 
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
  var message = '<%=pushResponse.getString("pushMessage") %>';
  var notification = new Notification('', {
    icon: 'http://imgur.com/5gGtgSX.png',
    body: message,
  });

  notification.onclick = function () {
    window.open("https://www.parts.renault.com/rpspub/aGetInternetPublicHomePageActionOut.do");      
  };
}

</script>
</head>
<body background="/WebContent/images/Renault.png" onload="shunno_push_api()">
<center><h1>PUSH NOTIFICATION CLIENT</h1>
<h2>Improving Sales Through Business Intelligence and Push Notifications</h2>
<img alt="Renault" src="http://www.underconsideration.com/brandnew/archives/renault_logo_detail.jpg">
<button onclick="shunno_push_api()" name="notify" value="Notify">

</center>
</body>
</html>