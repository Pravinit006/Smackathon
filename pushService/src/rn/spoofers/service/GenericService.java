package rn.spoofers.service;

import java.util.Date;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * @author z017185-dev, Logeswaran Veeramani
 * @since 2017
 * @category Web Service
 */

public class GenericService implements CommonConstant{
	protected boolean isR1 = false;
	protected boolean isR1LinkedToUser = false;
	protected boolean atleastOneValid = false;
	protected boolean isKeyAvailable = false;

	protected String errorCode = EMPTY_STRING;
	protected String r1GarageId = null;
	protected String countryCode = null;
	protected String productRef = null;
	protected String refErrorCode = null;
	protected String qtyErrorCode = null;
	protected int quantity = 0;
	
	
	protected String headerErrorCode = EMPTY_STRING;
	protected JSONObject servicePreferences = new JSONObject();
	protected String userIPN = null;
	protected String dealerID = null;
	protected String garageId = null;
	protected boolean r1DmsConnected = false;
	protected String countryZone=null;
	protected String origin=null;
	protected Date currentDate = null;

	protected boolean checkKeyIsAvailable(JSONObject jsonObj, String key, int option){
		try {
			switch(option){
			case OPTION_OBJECT:
				jsonObj.getJSONObject(key);
				isKeyAvailable= true;
				break;
			case OPTION_STRING:
				jsonObj.getString(key);
				isKeyAvailable= true;
				break;
			case OPTION_ARRAY:
				jsonObj.getJSONArray(key);
				isKeyAvailable= true;
				break;
			case OPTION_INT:
				jsonObj.getInt(key);
				isKeyAvailable= true;
				break;
			case OPTION_DOUBLE:
				jsonObj.getDouble(key);
				isKeyAvailable= true;
				break;
			case OPTION_BOOLEAN:
				jsonObj.getBoolean(key);
				isKeyAvailable= true;
				break;
			case OPTION_LONG:
				jsonObj.getLong(key);
				isKeyAvailable= true;
				break;
			default:
				isKeyAvailable= false;		
			}
		} catch (JSONException e) {
			isKeyAvailable= false;
		}
		return isKeyAvailable;
	}

	protected boolean checkKeyIsAvailable(JSONArray jsonObj, int index, String key, int option){
		try {
			switch(option){
			case OPTION_OBJECT:
				jsonObj.getJSONObject(index).getJSONObject(key);
				isKeyAvailable= true;
				break;
			case OPTION_STRING:
				jsonObj.getJSONObject(index).getString(key);
				isKeyAvailable= true;
				break;
			case OPTION_ARRAY:
				jsonObj.getJSONObject(index).getJSONArray(key);
				isKeyAvailable= true;
				break;
			case OPTION_INT:
				jsonObj.getJSONObject(index).getInt(key);
				isKeyAvailable= true;
				break;
			case OPTION_DOUBLE:
				jsonObj.getJSONObject(index).getDouble(key);
				isKeyAvailable= true;
				break;
			case OPTION_BOOLEAN:
				jsonObj.getJSONObject(index).getBoolean(key);
				isKeyAvailable= true;
				break;
			case OPTION_LONG:
				jsonObj.getJSONObject(index).getLong(key);
				isKeyAvailable= true;
				break;
			default:
				isKeyAvailable= false;		
			}
		} catch (JSONException e) {
			isKeyAvailable= false;
		}
		return isKeyAvailable;
	}
}