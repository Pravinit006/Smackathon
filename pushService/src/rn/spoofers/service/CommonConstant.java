package rn.spoofers.service;

public interface CommonConstant {
	
	/* Keys for stock */
	public static final String JSON_KEY_SERVICE_PREFERENCES = "ServicePreferences";
	public static final String JSON_KEY_USER_IPN = "UserId";
	public static final String JSON_KEY_R1GARAGE_ID = "DealerId";
	public static final String JSON_KEY_COUNTRY_CODE = "CountryCode";
	public static final String JSON_KEY_ERRORCODE = "ReturnCode";
	public static final String JSON_KEY_STOCK_LIST = "StockList";
	public static final String JSON_LIST_KEY_PRODUCT_REF = "Reference";
	public static final String JSON_LIST_KEY_QUANTITY = "WantedQuantity";
	public static final String JSON_LIST_KEY_ITEM_RETURN_CODE = "ItemReturnCode";
	public static final String JSON_LIST_KEY_HAS_ROUNDEDQTY = "HasRoundedQty";
	public static final String JSON_LIST_KEY_IS_IN_ANOMALY = "IsInAnomaly";
	public static final String JSON_LIST_KEY_ITEM_AVAILABILITY_STATUS_CODE = "ItemAvailabilityStatusCode";
	public static final String JSON_LIST_KEY_ITEM_AVAILABILITY_STATUS_NAME = "ItemAvailabilityStatusName";
	public static final String JSON_LIST_KEY_AVAILABLE_QTY_IN_DMS = "AvailableQuantity";
	
	public static final String JSON_KEY_APP_ID = "appId";
	public static final String JSON_KEY_USER_ID = "userId";
	public static final String JSON_KEY_PROD_ID = "prodId";
	public static final String JSON_KEY_VISIT_COUNT = "visitCount";
	public static final String JSON_KEY_ORDERED_QTY = "orderedQty";
	public static final String JSON_KEY_TOTAL_PRICE = "totalPrice";
	public static final String JSON_KEY_DISCOUNT_ID = "discountId";
	
	public static final String JSON_KEY_GARAGE_ID = "GarageId";
	public static final String JSON_KEY_PRICE_LIST = "PriceList";
	public static final String JSON_KEY_SIA = "Sia";
	//public static final String JSON_KEY_ORIGIN = "Origin";
	public static final String JSON_LIST_KEY_PRODUCT_PRICE = "Price";
	public static final String JSON_LIST_KEY_PRODUCT_SPARC_DISCOUNT = "SPARCDiscount";
	public static final String JSON_LIST_KEY_PRODUCT_SPARC_DISCOUNT_CODE = "SPARCDiscountCode";
	public static final String JSON_LIST_KEY_PRODUCT_DMS_DISCOUNT = "DMSDiscount";
	public static final String JSON_LIST_KEY_PRODUCT_DMS_DISCOUNT_CODE = "DMSDiscountCode";
	public static final String JSON_LIST_KEY_CURRENCY_CODE = "Currency";
	public static final String JSON_LIST_KEY_PCL_PRICE ="PCLPrice";
	public static final String JSON_LIST_KEY_PNC_PRICE ="PNCPrice";
	public static final String JSON_LIST_KEY_DMS_PRICE ="DMSPrice";
	//public static final String JSON_LIST_KEY_ANOMALYCODE ="AnomalyCode";
	
	public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int SEVEN = 7;
    public static final int EIGHT = 8;
    public static final int TEN = 10;
    public static final int SEVENTEEN = 17;
    public static final int TWENTY = 20;
    public static final int MAX_INT = 999;
	public static final String YES = "Y";
	public static final String EMPTY_STRING = "";
	public static final String NA = "NA";
	public static final int OPTION_OBJECT = 1;
	public static final int OPTION_STRING = 2;
	public static final int OPTION_ARRAY = 3;
	public static final int OPTION_INT = 4;
	public static final int OPTION_DOUBLE = 5;
	public static final int OPTION_BOOLEAN = 6;
	public static final int OPTION_LONG = 7;
	//public static final String SEPERATOR = ":";
	//public static final String HYPEN = "-";
	
	public static final String VALID_REQUEST = "H000";
	public static final String INVALID_ORIGINE_CODE = "H001";
	public static final String ERROR_INVALID_JSON_FORMAT = "H002"; 
	public static final String ERROR_USERIPN_EMPTY = "H003";
	public static final String ERROR_USER_NOT_IN_LDAP = "H004";
	public static final String ERROR_R1GARAGEID_EMPTY = "H005";
	public static final String ERROR_INVALID_R1GARAGEID = "H006";
	public static final String ERROR_USER_NOT_LINKED_TO_R1 = "H007";
	public static final String ERROR_EMPTY_LIST = "H008";
	public static final String ERROR_INVALID_LIST = "H009";
	public static final String ERROR_PARTIALLY_INVALID_LIST = "H010";
	public static final String ERROR_USER_IS_R1 = "H011";
	public static final String EXCEPTION = "H012";
	public static final String USER_DMS_UNAVAILABLE = "H013";
	public static final String NO_AUTOMATIC_VALIDATION = "H014";
	public static final String NO_DMS_FINANSTATUSSTATE = "H015";
	public static final String NO_MAXORDAMT = "H016";
	public static final String NO_MAXINVAMT = "H017";
	public static final String ERROR_INVALID_GARAGEID = "H018";
	public static final String LOCAL_EXCEPTION = "H021";
	public static final String CENTRAL_EXCEPTION = "H022";
	public static final String ERROR_USER_NOT_IN_RPS = "H023";
	public static final String LIST_MAX_SIZE_EXCEEDED = "H024";
	public static final String ERROR_NON_INTERFACED_USER = "H025";
	
	public static final String ERROR_SERVICEPREFERENCE_KEY_NOT_FOUND = "H026";
	public static final String ERROR_USERIPN_KEY_NOT_FOUND = "H027";
	public static final String ERROR_ORIGINE_KEY_NOT_FOUND = "H028";
	public static final String ERROR_R1GARAGEID_KEY_NOT_FOUND = "H029";
	public static final String ERROR_STOCK_LIST_KEY_NOT_FOUND = "H030"; 
	public static final String ERROR_GARAGEID_KEY_NOT_FOUND = "H031";
	public static final String ERROR_PRICE_LIST_KEY_NOT_FOUND = "H032";
	
	public static final String NO_ERROR = "A000";
	public static final String ERROR_EMPTY_REF = "A001";
	public static final String ERROR_INVALID_PRODUCTREF = "A002";
	public static final String ERROR_EMPTY_QTY = "A004";
	public static final String ERROR_INVALID_QUANTITY = "A005";
	
	/*Stock Availability codes start - Do not alter*/
	public static final String A006 = "A006"; // A006 - Not available in DMS - 1
	public static final String A007 = "A007"; // A007 - Available in DMS - 2
	public static final String A008 = "A008"; // A008 - Not available in store or Anomaly - 3
	public static final String A009 = "A009"; // A009 - Available in the central store - 4
	public static final String A010 = "A010"; // A010 - Available CDP - 5
	public static final String A011 = "A011"; // A011 - Not available due to anomaly - 6
	public static final String A012 = "A012"; // A012 - Not available due to service closure - 7
	public static final String A013 = "A013"; // A013 - Available CDPR  - 8
	public static final String A014 = "A014"; // A014 - N / A - (0 or -1)
	public static final String A015 = "A015"; // A015 - Anomaly in Reference/Quantity - So this line is not sent to DMS/ Central
	/*Stock Availability codes end - Do not alter*/
	public static final String ERROR_DEALER_DO_NOT_SELL_THIS_PRODUCT_OR_NON_COMMERCIALIZED = "A016";
	public static final String ERROR_PRODUCT_UNKNOWN = "A017";
	public static final String ERROR_PRODUCT_EXCLUDED = "A019";
	public static final String ERROR_DUPLICATE_PRODUCT = "A020";
	
	
	public static final String ERROR_IS_NOT_FOUND = "ReferenceNotFound";
	public static final String PRIMARY = "Primary";
	public static final String GARAGEID = "garageid";
	public static final String COUNTRY = "country";
	public static final String PROFILE_GARAGE = "profilelink";
	public final static String ENTITY_KIND = "entitykind";
	public static final String R1_R2 = "R0";
	public static final String R2MRA = "R2MRA";
	public static final String LANGUAGE = "preferredlanguage";
	public static final String FIRST_NAME = "givenname";
	public static final String LAST_NAME = "sn";
	public static final String PRODUCTREF = "PRODUCTREF";
	public static final String PRODUCTCOUNT = "PRODUCTCOUNT";
	public static final String ACTUALINVOICESAMOUNT = "ACTUALINVOICESAMOUNT";
	public static final String MAXINVOICESAMOUNT = "MAXINVOICESAMOUNT";
	public static final String MAXORDERAMOUNT = "MAXORDERAMOUNT";
	public static final String ZIPCODE = "ZIPCODE";
	public static final String CODEPR = "CODEPR";
	public static final String LOCALNUMBER = "LOCALNBR";
	public static final String APOLOCODE = "APOLOCODE";
	public static final String DMSNBR = "DMSNBR";
	public static final String DELIVERYOPTION = "DELIVERYOPTION";
	public static final String IS_Y = "Y";
	public static final String ISR1 = "ISR1";
	public static final String LANGUAGECODE = "LANGUAGECODE";
	public static final String COUNTRYCODE = "COUNTRYCODE";
	public static final String CONTACTLASTNAME = "CONTACTLASTNAME";
	public static final String CONTACTFIRSTNAME = "CONTACTFIRSTNAME";
	public static final String BUSINESSNAME = "BUSINESSNAME";
	public static final String DMSFINANCIALSTATUSSTATE ="DMSFINANCIALSTATUSSTATE";
	public static final String AUTOMATICORDERTRANSFERFLAG ="AUTOMATICORDERTRANSFERFLAG";
	
	public static final String ELIGIBLEINVOICEAMT = "EligibleInvoiceAmount";
	public static final String MAXORDERAMOUNT_RSP = "MaxmimumOrderAmount";
	public static final String MAXINVOICESAMOUNT_RSP = "MaximumInvoiceAmount";
	
	/** R2_R3 Order service : json key */
	public static final String JSON_KEY_ORDERTYPE = "OrderType";
	public static final String JSON_KEY_ORIGIN = "Sia";
	public static final String JSON_KEY_COMMENT = "Comment";
	public static final String JSON_KEY_PROMOTION_CODE = "AdvantageCode";
	public static final String JSON_KEY_STOCKIST = "Stockist";
	public static final String JSON_KEY_PRODUCT_LIST = "ProductList";
	public static final String JSON_KEY_QUANTITY = "Quantity";
	public static final String JSON_KEY_ANOMALY_CODE = "ItemReturnCode";
	public static final String JSON_KEY_ORDER_STATUS_NUM = "OrderStatusNumber";
	public static final String JSON_KEY_TOTAL = "Total";
	public static final String JSON_KEY_CURRENCY = "Currency";
	public static final String JSON_KEY_ORDER_DATE = "OrderDate";
	public static final String JSON_KEY_CALLER_ORDER_ID = "CallerOrderId";
	public static final String JSON_KEY_ORDER_ID = "OrderId";
	public static final String JSON_KEY_APPLICATION = "Application";
	public static final String JSON_KEY_RPS = "RPS";
	public static final String JSON_KEY_ORDERIDLIST = "OrderIdList";
	public static final String JSON_KEY_DELIVERYOPTION = "DeliveryOption";
	public static final String JSON_KEY_ORDPERSONALREFERENCE = "OrderPersonalReference";
	public static final String JSON_KEY_CLIENTCODE = "ClientCode";
	public static final String JSON_KEY_STOCKTYPE = "StockType";
	public static final String JSON_KEY_AVAILSTATUS = "AvailablilityStatus";
	public static final String JSON_KEY_ITEMPERSONALREFERENCE = "ItemPersonalReference";
	public static final int ORDERTYPE_99_INT = 99;
	public static final String ST = "ST";
	public static final String UR = "UR";
	
	/** R2_R3 Order service : return code */
	public static final String VALID_REQUEST_RESPONSE_LABEL = "H000";
	public static final String INVALID_JSON_FORMAT_LABEL = "H001";
	public static final String SERVICE_PREFERENCES_IS_NOT_PRESENT_EMPTY_LABEL = "H002";
	public static final String IPN_IS_NOT_PRESENT_EMPTY_LABEL = "H003";
	public static final String IPN_NOT_PRESENT_IN_LDAP_LABEL = "H004";
	public static final String USER_DONT_HAVE_ORDER_PROFILE_LABEL = "H005";
	public static final String USER_IS_DEALER_LABEL = "H006";
	public static final String DEALER_ID_INVALID_NOT_PRESENT_EMPTY_LABEL = "H007";
	public static final String NON_INTERFACED_LABEL = "H008";
	public static final String CALLER_ORDER_ID_IS_INVALID_NOT_PRESENT_EMPTY_LABEL = "H009";
	public static final String ORIGIN_INVALID_NOT_PRESENT_EMPTY_LABEL = "H010";
	public static final String STOCKIST_INVALID_NOT_PRESENT_EMPTY_LABEL = "H011";
	public static final String CLIENTCODE_IS_INVALID_NOT_PRESENT_EMPTY_LABEL = "H012";
	public static final String ORDERTYPE_INVALID_NOT_PRESENT_EMPTY_LABEL = "H013";
	public static final String LIST_INVALID_NOT_PRESENT_EMPTY_LABEL = "H014";
	public static final String PARTIALLY_VALID_LIST_LABEL = "H015";
	public static final String PRODUCT_LIST_COUNT_EXCEEDED_LABEL = "H016";
	public static final String DUPLICATE_REFERENCE_LIST_LABEL = "H017";
	public static final String IS_NOT_HAVING_AUTOMATIC_VALIDATION_LABEL = "H018";
	public static final String ORDER_MAXIMUM_AMOUNT_EXCEEDED_LABEL = "H019";
	public static final String MAXIMUM_BILLING_AMOUNT_EXCEEDED_LABEL = "H020";
	public static final String ORDER_PROCESSED_ALREADY_LABEL = "H021";
	public static final String CODE_PR_NULL_OR_EMPTY_LABEL = "H022";
	public static final String MSG_IS_AVAILABLE_LABEL = "H023";
	public static final String GARAGE_ACCOUNT_DETAILS_NOT_PRESENT_UNDER_DEALER_IN_DB_LABEL = "H024";
	public static final String USER_GARAGE_NOT_IN_RPS_DB = "H034";
	
	/** R2_R3 Order service : item return code */
	public static final String VALID_ELEMENT_LABEL = "A000";
	public static final String REFERENCE_INVALID_NOT_PRESENT_EMPTY_LABEL = "A001";
	public static final String PRODUCT_NOT_LINKED_TO_SEGMENT_LABEL = "A002";
	public static final String QUANTITY_INVALID_NOT_PRESENT_EMPTY_LABEL = "A003";
	public static final String DUPLICATE_REFERENCE_LABEL = "A004";
	public static final String ORDER_DATE_INVALID_NOT_PRESENT_EMPTY_LABEL = "A005";
	public static final String PRODUCT_NOT_LINKED_TO_MARKETING_FAMILY_LABEL = "A006";
	public static final String PRODUCT_EXCLUDED_LABEL = "A007";
	public static final String DEALER_DO_NOT_SELL_THIS_PRODUCT_OR_NON_COMMERC_LABEL = "A008";
	public static final String PRODUCT_UNKNOWN_LABEL = "A009";
	public static final String REFERENCE_LENGTH_IS_TEN_OR_SPL_CHAR_LABEL = "A010"; 
	
	/*DEALER order service*/
	public static final String USER_IS_NOT_R1 = "H028";
	public static final String ORDER_EXCEPTION = "H033";
	public static final String NORMAL = "N";
	public static final String GROSSITE = "G";
	
}