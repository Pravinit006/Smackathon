package rn.spoofers.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.xml.rpc.ServiceException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * @author z07185-dev Logeswaran Veeramani
 *
 */
@Path("/pushService")
public class PushService extends GenericService {
	Connection connection = null;
	private static Map<String, Object> propertyFileMap = new HashMap<String, Object>();
	private static final String fileName = "conf\\smackathon.properties";
	private static final String CONNECTER = " - ";
	private static final String DB_URL = "smackathon.db.url";
	private static final String USERNAME = "smackathon.db.username";
	private static final String PASSWORD = "smackathon.db.password";
	@POST 
	@Produces("application/json")
	public JSONObject pushMessageService(JSONObject pushRequest)throws ServiceException {

		//String query = "select SU.I_APPL_ID, SU.USER_ID ,PS.PROD_ID, PS.MESS , 'N' from TB_SUBSCRIBEHDR SU ,temp_push PS where SU.SUBSCRIBE='Y' and SU.I_APPL_ID=PS.I_APPL_ID";
		JSONObject pushResponse = new JSONObject();
		try{
			String appId = pushRequest.getString(JSON_KEY_APP_ID);
			String userId = pushRequest.getString(JSON_KEY_USER_ID);
			if(isUserAvailable(appId,userId)){
				String message = fetchPushMessage(appId,userId);
				String product = fetchTopProduct(appId,userId);
				pushResponse.put("pushMessage", message+CONNECTER+product);
			}
		}catch(JSONException je){

		}
		return pushResponse;
	}
	private boolean isUserAvailable(String appId, String userId){
		boolean isValid = false;
		PreparedStatement statement = null;
		ResultSet  userInfoRS = null;
		StringBuffer buffer  = new StringBuffer();
		buffer.append("SELECT USER_ID from TB_SUBSCRIBEHDR where SUBSCRIBE='Y' and I_APPL_ID= ? AND USER_ID= ?");
		try {
			statement = (PreparedStatement) getConnection().prepareStatement(buffer.toString());
			statement.setString(1, appId);
			statement.setString(2, userId);
			userInfoRS = statement.executeQuery();
			if(userInfoRS.next()){
				isValid = true;
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			statement = null;
			userInfoRS = null;
			}
		return isValid;
	}
	private String fetchPushMessage(String appId, String userId){
		String pushMessage = EMPTY_STRING;
		PreparedStatement statement = null;
		ResultSet  pushInfo = null;
		StringBuffer buffer  = new StringBuffer();
		buffer.append("SELECT MESS from TB_PUSH_NOTIFY_HDR where I_APPL_ID=? AND USER_ID=?");
		try {
			statement = (PreparedStatement) getConnection().prepareStatement(buffer.toString());
			statement.setString(1, appId);
			statement.setString(2, userId);
			pushInfo = statement.executeQuery();
			if(pushInfo.next()){
				pushMessage = pushInfo.getString("MESS");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			statement = null;
			pushInfo = null;
			}
		return pushMessage;
	}
	private String fetchTopProduct(String appId, String userId){
		String product = EMPTY_STRING;
		PreparedStatement statement = null;
		ResultSet  pushInfo = null;
		StringBuffer buffer  = new StringBuffer();
		buffer.append("SELECT PROD_ID from TB_PUSH_NOTIFY_HDR where I_APPL_ID=? AND USER_ID=?");
		try {
			statement = getConnection().prepareStatement(buffer.toString());
			statement.setString(1, appId);
			statement.setString(2, userId);
			pushInfo = statement.executeQuery();
			if(pushInfo.next()){
				product = pushInfo.getString("PROD_ID");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			statement = null;
			pushInfo = null;
		}
		return product;
	}
	private Connection getConnection(){
		synchronized (PushService.class) {
			if(connection == null){
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					try {
						connection = DriverManager.getConnection(getDBUrl(),getUserName(),getPassword());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return connection;
	}
	private String getPassword() {
		// TODO Auto-generated method stub
		return (String) propertyFileMap.get(PASSWORD);
	}
	private String getUserName() {
		// TODO Auto-generated method stub
		return (String) propertyFileMap.get(USERNAME);
	}
	private String getDBUrl() {
		// TODO Auto-generated method stub
		return (String) propertyFileMap.get(DB_URL);
	}
	
	private static void getPropertyBundle(){
		FileInputStream configurationFile;
		try{
			configurationFile = new FileInputStream(new File(fileName));
			if(configurationFile != null){
				PropertyResourceBundle bundle = new PropertyResourceBundle(configurationFile);
				populateParamMap(bundle, DB_URL);
				populateParamMap(bundle, USERNAME);
				populateParamMap(bundle, PASSWORD);
			}
		}catch (FileNotFoundException e) {
			System.out.println("Configuration file not found");
		} catch (IOException e) {
			System.out.println("Configuration file not found");
		}
	}
	private static void populateParamMap(PropertyResourceBundle bundle, String key) {
		String valueAsStr = bundle.getString(key);
		if(key != null){
			propertyFileMap.put(key, valueAsStr);
		}
	}
}